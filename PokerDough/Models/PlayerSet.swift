//
//  PlayerSet.swift
//  PokerDough
//
//  Created by Neil Schreiber on 1/8/20.
//  Copyright © 2020 Neil Schreiber. All rights reserved.
//

import Foundation

struct PlayerSet: Identifiable, Codable {
    let id = UUID()
    var name: String
    var items: [LedgerItem]
    
    var sum: Int {
        items.reduce(0, {$0 + $1.amount})
    }
}
