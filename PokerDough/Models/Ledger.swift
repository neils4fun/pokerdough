//
//  Ledger.swift
//  PokerDough
//
//  Created by Neil Schreiber on 1/8/20.
//  Copyright © 2020 Neil Schreiber. All rights reserved.
//

import Foundation

class Ledger: ObservableObject {
    @Published var players: [PlayerSet] {
        didSet {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(players) {
//                print("Saved ledger: \(String(decoding: encoded, as: UTF8.self))")
                UserDefaults.standard.set(encoded, forKey: "Ledger")
            }
        }
    }
    
    var playerNames: [String] {
        players.map { $0.name }.sorted(by:
        {
            return $0 < $1
        })
    }
    
    init() {
        if let players = UserDefaults.standard.data(forKey: "Ledger") {
            let decoder = JSONDecoder()
            if let decoded = try? decoder.decode([PlayerSet].self, from: players) {
                self.players = decoded
                return
            }
        }

        self.players = []
    }
    
    func clear() {
        self.players = []
    }
    
    func addToLedger(player: String, amount: Int) {
        // Lookup this player in the ledger
        if let playerSetIndex = lookupIndex(player) {
//            print("Found playerSet for \(player)")
            // If found, update this playerSet (by removing the old playerSet and adding the new one)
            var oldPlayerSetItems = players[playerSetIndex].items
            oldPlayerSetItems.append(LedgerItem(amount: amount))
            let newPlayerSet = PlayerSet(name: player, items: oldPlayerSetItems)
            self.players.remove(at: playerSetIndex)
            self.players.append(newPlayerSet)
        } else {
            // If not found, append a new playerSet
//            print("Did not find playerSet for \(player)")
            var items = [LedgerItem]()
            items.append(LedgerItem(amount: amount))
            let newPlayerSet = PlayerSet(name: player, items: items)
            self.players.append(newPlayerSet)
        }
    }
    
    func lookupIndex(_ player: String) -> Int? {
        return players.firstIndex(where: {$0.name == player})
    }
    
    func grandTotal() -> Int {
        return players.reduce(0, {$0 + Int($1.sum)})
    }
    
}
