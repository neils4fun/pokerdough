//
//  LedgerItem.swift
//  PokerDough
//
//  Created by Neil Schreiber on 1/8/20.
//  Copyright © 2020 Neil Schreiber. All rights reserved.
//

import Foundation

struct LedgerItem: Identifiable, Codable {
    let id = UUID()
    var amount: Int
}

