//
//  ContentView.swift
//  PokerDough
//
//  Created by Neil Schreiber on 1/6/20.
//  Copyright © 2020 Neil Schreiber. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var ledger = Ledger()
    
    @State var showConfirmClearAll = false
    
    @State private var showingAddEntry = false
    
    var body: some View {
        let sortedEntries = sortLedger()
        
        return NavigationView {
            VStack {
                List {
                    ForEach(sortedEntries) { playerSet in
                        Section( header: HStack {
                            Text("Player: \(playerSet.name)").fontWeight(.bold).font(.headline)
                                Spacer()
                            Text("Total: \(playerSet.sum)")
                                .fontWeight(.bold)
                                .font(.headline)
                                .foregroundColor(playerSet.sum < 0 ? .red : .black)
                        }) {
                            ForEach(playerSet.items) { ledgerItem in
                                HStack {
                                    Spacer()
                                    Text("\(ledgerItem.amount)")
                                        .foregroundColor(ledgerItem.amount < 0 ? .red : .black)
                                }
                            }
                            .onDelete { self.remove(rows: $0, player: playerSet.name) }
                        }
                    }
                    
                    HStack {
                        Text("Grand Total")
                        Spacer()
                        Text("\(ledger.grandTotal())")
                            .fontWeight(.bold)
                            .font(.headline)
                            .foregroundColor(ledger.grandTotal() < 0 ? .red : .black)
                    }
                }
            }
            .navigationBarItems(
                leading:
                Button("Clear All") {
                    self.showConfirmClearAll = true
                }
                .alert(isPresented:$showConfirmClearAll) {
                    Alert(title: Text("Are you sure you want to clear all entries?"), message: Text(" They will be permanently deleted, there is no undo!"), primaryButton: .destructive(Text("Delete")) {
//                        print("Deleting ledger....")
                        self.ledger.clear()
                        }, secondaryButton: .cancel())
                },
                trailing:
                Button(action: {
                    self.showingAddEntry = true
                }) {
                    Image(systemName: "plus")
                }
            )
            .navigationBarTitle("Poker Dough")
        }
        .onAppear {
//            UserDefaults.standard.removeObject(forKey: "Ledger")
//            print("View did appear")
        }
        .sheet(isPresented: $showingAddEntry) {
            AddEntry(ledger: self.ledger)
        }
    }
    
    func remove(rows offsets: IndexSet, player: String) {
//        print("Remove offsets \(offsets.first!) for \(player)")
        if let playerSetIndex = ledger.lookupIndex(player) {
            var oldPlayerSetItems = ledger.players[playerSetIndex].items
            oldPlayerSetItems.remove(at: offsets.first!)
            let newPlayerSet = PlayerSet(name: player, items: oldPlayerSetItems)
            self.ledger.players.remove(at: playerSetIndex)
            
            // If no indices then remove then just leave this playerSet out
            if !oldPlayerSetItems.isEmpty {
                self.ledger.players.append(newPlayerSet)
            }
        }
    }
    
    func sortLedger() -> [PlayerSet] {
        // Sort the PlayerSets by the name
        let sortedLedger = ledger.players.sorted(by:
        {
            return $0.name < $1.name
        })
        
        // Sort each PlayerSet by amount
//        if $0.name == $1.name {
//            return $0.amount > $1.amount
//        }

        return sortedLedger
    }
    
    func cancel() {
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
