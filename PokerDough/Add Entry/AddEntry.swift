//
//  AddEntry.swift
//  PokerDough
//
//  Created by Neil Schreiber on 1/8/20.
//  Copyright © 2020 Neil Schreiber. All rights reserved.
//

import SwiftUI

struct AddEntry: View {
    @ObservedObject var ledger: Ledger
    
    @Environment(\.presentationMode) var presentationMode

    @State private var playerName = ""
    @State private var selectedPlayer = 0
    @State private var amount = ""

    var signs = ["Credit", "Debit"]
    @State var selectedSign = 1

    var body: some View {
        NavigationView {
            Form {
                TextField("Enter a New Player Name", text: self.$playerName)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                Picker("Select a Player", selection: $selectedPlayer) {
                    ForEach(0 ..< ledger.playerNames.count) {
                        Text(self.ledger.playerNames[$0])
                    }
                }
                TextField("Enter Amount", text: self.$amount)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .keyboardType(.numberPad)
                Picker("Type", selection: $selectedSign) {
                    ForEach(0 ..< signs.count) {
                        Text(self.signs[$0])
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
            }
            .navigationBarTitle("Add new entry")
            .navigationBarItems(trailing: Button("Save") {
                self.addEntry()
            })
        }
    }
    
    func addEntry() {
        var amount = Int(self.amount) ?? 0
        amount *= (self.selectedSign == 0 ?  1 : -1)
        
        print("Add Entry \(amount)")

        var playerName = self.playerName
        if playerName.isEmpty {
            playerName = ledger.playerNames[self.selectedPlayer]
        }
        
        ledger.addToLedger(player: playerName, amount: amount)
        
        self.presentationMode.wrappedValue.dismiss()
//        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
    }
}

struct AddEntry_Previews: PreviewProvider {
    static var previews: some View {
        AddEntry(ledger: Ledger())
    }
}
